<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: category_discount.php 15 2008-12-24 20:09:30Z numinix $
 */
 
include(DIR_WS_MODULES . 'order_total/ot_category_discount.php');
$category_discount = new ot_category_discount();
$discount = $category_discount->get_total_product_discount($_SESSION['cart']->get_products(), MODULE_CATEGORY_DISCOUNT_TYPE);

?>
<?php if ($discount > 0) { ?>
<div id="cartInstructionsDisplay" class="content">
<?php 
  echo '<p>' . sprintf(TEXT_CATEGORY_DISCOUNT, strtolower(MODULE_CATEGORY_DISCOUNT_TYPE), $currencies->format($discount), zen_href_link(FILENAME_CHECKOUT, '', 'SSL')) . '</p>'; 
  if (DISPLAY_PRICE_WITH_TAX == 'true') {
    echo '<p>Note: Discount is calculated on price before taxes. Product\'s prices are displayed with tax.</p>';
  }
?>
</div>
<?php } ?>
<?php
if (MODULE_CATEGORY_DISCOUNT_ALERT == 'true') {    
  if ($discount > 0) {
    $product_ids = $category_discount->product_ids; 
    function alert($msg) { 
      $msg = addslashes($msg); 
      $msg = str_replace("\n", "\\n", $msg); 
      echo "<script language='javascript'><!--\n"; 
      echo 'alert("' . $msg . '")'; 
      echo "//--></script>\n\n"; 
    }
    
    foreach ($product_ids as $products_id => $sum) {
      if ($products_id != '') {
        $products_query = "SELECT products_name FROM " . TABLE_PRODUCTS_DESCRIPTION . " 
                           WHERE products_id = '" . (int)$products_id . "'
                           AND language_id = '" . $_SESSION['languages_id'] . "' 
                           LIMIT 1";
        $products = $db->Execute($products_query);
        $product_names_msg .= $products->fields['products_name'] . "\n";
      }
    }

    $msg = "Your purchase is eligible for a " . strtolower(MODULE_CATEGORY_DISCOUNT_TYPE) . " sensitive discount of " . $currencies->format($discount) . "! \nThe discount will be reflected during the checkout and is calculated based on the following products:\n\n" . $product_names_msg;
    if ($_SESSION['discount'] != $discount) {
      alert($msg);
    }   
    $_SESSION['discount'] = $discount;
  }  
}
?>