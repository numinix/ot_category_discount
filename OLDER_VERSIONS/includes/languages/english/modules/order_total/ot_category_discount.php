<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_category_discount.php 10 2008-06-02 14:30:11Z numinix $
 */
  if (IS_ADMIN_FLAG) {
    define('MODULE_CATEGORY_DISCOUNT_TITLE', 'Category Discount');
    define('MODULE_CATEGORY_DISCOUNT_DESCRIPTION', 'Category Discount');
  } else {
    define('MODULE_CATEGORY_DISCOUNT_TITLE', ucwords(MODULE_CATEGORY_DISCOUNT_TYPE) . ' Discount');
    define('MODULE_CATEGORY_DISCOUNT_DESCRIPTION', ucwords(MODULE_CATEGORY_DISCOUNT_TYPE) . ' Discount');
  }
  define('SHIPPING_NOT_INCLUDED', ' [Shipping not included]');
  define('TAX_NOT_INCLUDED', ' [Tax not included]');
?>