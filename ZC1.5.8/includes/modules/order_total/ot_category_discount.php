<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_category_discount.php 17 2011-05-05 03:42:10Z numinix $
 */

  class ot_category_discount {
    var $title, $output, $discount_categories, $categories_switch, 
        $code, $description, $enabled, $sort_order,
        $table, $groups, $total_percentage, $mode, $product_ids,
        $discount_tables, $category_wide_table_id, $category_wide,
        $table_id,$tax_discount;
    function __construct() {
      
      $this->groups = (defined('MODULE_CATEGORY_DISCOUNT_GROUPS')) ? (int) MODULE_CATEGORY_DISCOUNT_GROUPS : 3;
      // $this->install();
      $this->code = 'ot_category_discount';
      $this->title = MODULE_CATEGORY_DISCOUNT_TITLE;
      $this->description = MODULE_CATEGORY_DISCOUNT_DESCRIPTION;
      $this->enabled = (defined('MODULE_CATEGORY_DISCOUNT_STATUS')) ? ((MODULE_CATEGORY_DISCOUNT_STATUS == 'true') ? true : false) : false;
      $this->wholesale = (isset($_SESSION['customer_whole']) && $_SESSION['customer_whole'] > 0) ? '_W' : '';
      $this->sort_order = (defined('MODULE_CATEGORY_DISCOUNT_SORT_ORDER')) ? MODULE_CATEGORY_DISCOUNT_SORT_ORDER : null;
      $this->table = "";//$this->table = MODULE_CATEGORY_DISCOUNT_TABLE;
      $this->discount_type = (defined('MODULE_CATEGORY_DISCOUNT_CALC_TYPE' . $this->wholesale)) ? constant('MODULE_CATEGORY_DISCOUNT_CALC_TYPE' . $this->wholesale) : "";
      $this->output = array();
      $this->discount_categories = array();
      $this->recalculate_tax = (defined('MODULE_CATEGORY_DISCOUNT_CALC_TAX')) ? MODULE_CATEGORY_DISCOUNT_CALC_TAX : "";
      $this->mode = (defined('MODULE_CATEGORY_CALCULATION_TYPE' . $this->wholesale)) ? constant('MODULE_CATEGORY_CALCULATION_TYPE' . $this->wholesale) : "";
      if (!IS_ADMIN_FLAG) { 
        for ($j=1; $j<=$this->groups; $j++) {
          // create an array of category tables and discount tables for use within the script
          $this->discount_categories[$j] = constant('MODULE_CATEGORY_DISCOUNT_CATEGORIES_' . $j);
          $this->discount_tables[$j] = constant('MODULE_CATEGORY_DISCOUNT_TABLE_' . $j . $this->wholesale);
          // make sure that both the categories and the discounts table are populated
          if ($this->discount_categories[$j] != '' && $this->discount_tables[$j] != '') {
            $this->categories_switch = true;
          } elseif ($this->discount_categories[$j] == '' && $this->discount_tables[$j] != '' && $this->category_wide_table_id != $j) {
            $this->category_wide = true;
            $this->category_wide_table_id = $j;
          }
        }
      }
    }

    function process() {
      global $order, $ot_subtotal, $currencies, $db;
      
      if (!$this->enabled) return false;
      
      $products_array = $_SESSION['cart']->get_products();
      $categories_discount = $this->get_total_product_discount($products_array, constant('MODULE_CATEGORY_DISCOUNT_TYPE' . $this->wholesale));
      if ($categories_discount > 0) {
        $this->deduction = round($categories_discount, 2);
        $this->output[] = array('title' => $this->title . ':',
                                'text' => '-' . $currencies->format($this->deduction),
                                'value' => $this->deduction); 
        $order->info['total'] = $order->info['total'] - $this->deduction - $this->tax_discount;
        if ($this->sort_order < $ot_subtotal->sort_order) {
          $order->info['subtotal'] = $order->info['subtotal'] - $this->deduction;
        }
      }
      if ($this->recalculate_tax == 'true') {
        $order->info['tax'] = $order->info['tax'] - $this->tax_discount;
      }
      if (DISPLAY_PRICE_WITH_TAX == 'true') {
        $order->info['total'] += $this->tax_discount;
      }
    }
    
    function get_products_price($products_id) {
      $products_price = zen_get_products_actual_price($products_id);
      if (DISPLAY_PRICE_WITH_TAX == 'true') {
        // get tax for product
        $products_tax_class_id = $this->get_products_tax_class_id($products_id);
        $tax_rate = zen_get_tax_rate($products_tax_class_id);
        $tax = zen_calculate_tax($products_price, $tax_rate);
        $products_price += $tax;
      }
      return $products_price;
    }
    
    // returns true if product is eligible for discount
    // used outside of checkout
    function check_product_discount($products_id) {
      if ($this->get_table_id($products_id) != false) {
        return true;
      } else {
        return false;
      } 
    }
    
    // gets the table_id for which the product's category is defined
    // used outside of checkout
    function get_table_id($products_id) {
      if ($this->enabled) {
        if ($this->categories_switch == true) {
          $counter = 0;
          foreach($this->discount_categories as $categories_string) {
            $counter++;
            $categories_array = explode(',', $categories_string);
            foreach($categories_array as $category_id) {
              if (zen_product_in_category($products_id, $category_id)) {
                $this->table_id = $counter; 
                return $this->table_id;
              }
            }
          }
        } elseif ($this->category_wide == true) {
          // return the table id that contains the discount
          // assumes that if using site-wide discounts that only one discount_table would be populated.
          $this->table_id = $this->category_wide_table_id;
          return $this->table_id;
        }
      }
      return false;
    }  
    
    // used outside of checkout
    function create_discount_display_table($table_id) {
      global $currencies;
      // build array
      $table_costs = explode("," , constant('MODULE_CATEGORY_DISCOUNT_TABLE_' . $table_id . $this->wholesale)); // 0 => 1:2, 1 => 2:3, 2 => 3:4;
      //print_r($table_costs);
      $count = count($table_costs);
      for($i = 0; $i < $count; $i++) {
        $table_costs[$i] = explode(":", $table_costs[$i]); // 0 => (0 => 1, 1=> 2), 1 => (0 => 2, 1 => 3), 2 => (0 => 3, 1 => 4); 
      }
      // build display table
      $output =  "\n" . '<table id="categoryDiscountTable" border="0" cellspacing="0" cellpadding="5" width="260">' . "\n" .
                 '  <tr id="categoryDiscountTableHeaders">' . "\n" .
                 '    <th id="categoryDiscountTableCondition">';
                 
      $calculation_type = MODULE_CATEGORY_DISCOUNT_CALC_TYPE; // percentage or flat
      $discount_type = MODULE_CATEGORY_DISCOUNT_TYPE; // quantity or price
      
      if ($discount_type == 'price') {
        $output .= 'Order Total';
      } elseif ($discount_type == 'quantity') {
        $output .= 'Number of Items';
      }
      $output .= '</th>' . "\n" . 
                 '    <th id="categoryDiscountTableDiscount">Discount</th>' . "\n" . 
                 '  </tr>' . "\n";
      $counter = 0;
      $last_row = count($table_costs);
      foreach ($table_costs as $table_cost) {
        //print_r($table_cost);
        $counter++;
        //echo $calculation_type;
        if ($discount_type == 'quantity') {
          $condition = $table_cost[0];
        } elseif ($discount_type == 'price') {
          $condition = $currencies->format($table_cost[0]);
        }
        if ($counter == $last_row) {
          $condition .= '+';
        } else {
          // display as a currency
          if ($discount_type == 'price') {
            // subtract 1 unit of currency
            $condition_range = $table_costs[$counter][0] - 0.01;
            $condition_range = $currencies->format($condition_range);
          } else {
            // subtract 1 unit
            $condition_range = $table_costs[$counter][0] - 1;
          }
          // make sure current condition and next level are not within a single unit, after above calculation
          if ($condition != $condition_range) {
            $condition .= '-' . $condition_range;
          }
        }
        if ( $calculation_type== 'percentage') {
          $discount = $table_cost[1] . '%';
        } elseif ($calculation_type == 'flat') {
          $discount = $currencies->format($table_cost[1]);
        }
        // make sure something is being output in the row
        if ($condition != '' && $discount != '') {
          $output .= '  <tr class="' . $this->checkNum($counter) . '">' . "\n" . 
                     '    <td class="categoryDiscountCondition">' . $condition . '</td>' . "\n";
          $output .= '    <td class="categoryDiscountDiscount">' . $discount . '</td>' . "\n";
          $output .= '  </tr>' . "\n";
        } 
      }
      $output .= '</table>';
      return $output;
    }
    // used outside of checkout
    function checkNum($num){
      return ($num%2) ? 'categoryDiscountOddRow' : 'categoryDiscountEvenRow';
    }
    
    // returns the total discount for individual and sum modes
    // can be used inside or outside of checkout
    function get_total_product_discount($products_array, $switch) {
      global $order;
      $total_discount = 0;
      $product_ids = array();
      $products_measure_total = [];
      $products_price_sum = 0;
      for ($i=0; $i<count($products_array); $i++) {
        $products_id = $products_array[$i]['id'];
        if ($this->categories_switch == true) {
          $counter = 0;  
          foreach($this->discount_categories as $categories_string) {
            $counter++;
            $products_measure_total[$counter] = 0;
            $categories_array = explode(',', $categories_string);
            foreach($categories_array as $category_id) {
              if (zen_product_in_category($products_id, $category_id)) {            
                if ($this->mode == 'individual') {
                  // add the discounts for this product to the total discount
                  $total_discount += $this->get_product_discount($products_id, $counter, $switch);
                } elseif ($this->mode == 'sum') {
                  // create products price and quantity total
                  switch ($switch) {
                    case 'price':
                      $products_measure_total[$counter] += $this->get_products_price($products_id) * $_SESSION['cart']->contents[$products_id]['qty'];
                      $products_price_sum += $products_amount = $products_measure_total[$counter];
                      break;
                    case 'quantity':
                      $products_measure_total[$counter] += $_SESSION['cart']->contents[$products_id]['qty'];
                      $products_price_sum += $products_amount = $this->get_products_price($products_id) * $_SESSION['cart']->contents[$products_id]['qty'];
                      break;
                  }
                }
                // create array of products ids and total prices
                if(isset($products_amount)) $product_ids[$products_id] = $products_amount;
                // break all category checks as first table takes priority
                break 2;
              }
            }
          }
        // give discount on all products regardless of category  
        } elseif ($this->category_wide) {
          if ($this->mode == 'individual') {
            $total_discount += $this->get_product_discount($products_id, $this->category_wide_table_id, $switch);
          } elseif ($this->mode == 'sum') {
            switch ($switch) {
              case 'price':
                $products_price_sum = $products_measure_total[$this->category_wide_table_id] += $this->get_products_price($products_id) * $_SESSION['cart']->contents[$products_id]['qty'];
                break;
              case 'quantity':
                $products_measure_total[$this->category_wide_table_id] += $_SESSION['cart']->contents[$products_id]['qty'];
                $products_price_sum = $this->get_products_price($products_id) * $_SESSION['cart']->contents[$products_id]['qty'];
                break;
            } 
          }
        }
      }
      $this->product_ids = $product_ids;
      //print_r($product_ids);
      if ($this->mode == 'sum') {
        $total_discount = $this->get_sum_discount($products_measure_total, $products_price_sum, $switch);
        // tax would have already been recalculated in a sub function for mode = individual
        if ($this->recalculate_tax == 'true') {
          foreach($product_ids as $prod_id => $products_price_value) {
            $products_discount_ratio = $products_price_value / $products_price_sum;
            $products_discount = $products_discount_ratio * $total_discount;
            $this->tax_discount += $this->get_tax_discount($prod_id, $products_discount);
          }
        }
      }
      // add the tax to the discount if price is displaying with tax as discount is calculated on net price, not with tax
      //if (DISPLAY_PRICE_WITH_TAX == 'true') {
        //$total_discount += $this->tax_discount;
        // add the tax to the total as it will be subtracted again later
        //$order->info['total'] += $this->tax_discount;
      //}
      return $total_discount;
    }
    
    // calculates the total discount for a sum of products within each group
    function get_sum_discount($groups_total, $price_total, $switch) {
      $total_discount = 0;
      if (is_array($groups_total)) {
        foreach ($groups_total as $table_id => $group_total) {
          if ($this->discount_type == 'percentage') {
            $percentage = $this->get_percentage($group_total, $table_id); 
            $group_discount = $group_total * ($percentage / 100);
          } elseif ($this->discount_type == 'flat') {
            $group_discount = $flat = $this->get_flat($group_total, $table_id);
          }
          $total_discount += $group_discount;
        }
      }
      return $total_discount;
    }
    
    // calculates the tax deduction based on the size of the discount and the products tax class rate
    function get_tax_discount($products_id, $products_discount) {
      global $order;
      $products_tax_class_id = $this->get_products_tax_class_id($products_id);
      $tax_discount = 0;
      if (is_object($order->info['tax_groups'])) {
        reset($order->info['tax_groups']);
        while (list($key, $value) = each($order->info['tax_groups'])) {
          if ($this->get_tax_class_id_from_description($key) == $products_tax_class_id) {
            // make sure that tax is already calculated
            if ($order->info['tax_groups'][$key] > 0) {
              // calculate tax rate for each tax description and class combination
              $tax_rate = $this->get_tax_rate($products_tax_class_id, $key);
              //tax deduction is the amount being subtracted from the price multiplied by the tax rate
              $tax_deduction = $products_discount * $tax_rate;
              // subtract the tax for this tax rate type
              $order->info['tax_groups'][$key] -= $tax_deduction;
              // keep track of the total tax deductions for the order total
              $tax_discount += $tax_deduction;
            }
          }
        }
      }
      return $tax_discount;
    }
    
    // return the tax class id for a product
    function get_products_tax_class_id($products_id) {
      global $db;
      $products_query = "SELECT * FROM " . TABLE_PRODUCTS . " WHERE products_id = " . (int)$products_id . " LIMIT 1";
      $products = $db->Execute($products_query);
      $products_tax_class_id = $products->fields['products_tax_class_id'];
      return $products_tax_class_id;
    }
    
    // calculates the discount for each product and executes the tax calculation deduction       
    function get_product_discount($products_id, $table_id, $switch) {
      global $order, $db, $currencies;
      $products_price = $this->get_products_price($products_id);
      $products_qty = $_SESSION['cart']->contents[$products_id]['qty'];
      $products_total = $products_price * $products_qty;
      $products_query = "SELECT * FROM " . TABLE_PRODUCTS . " WHERE products_id = " . (int)$products_id . " LIMIT 1";
      $products = $db->Execute($products_query);
      $products_tax_class_id = $products->fields['products_tax_class_id'];
      $products_tax_rate = zen_get_tax_rate($products_tax_class_id);
      switch ($switch) {
        case 'price':
          if ($this->discount_type == 'percentage') {
            $percentage = $this->get_percentage($products_total, $table_id);
          } elseif ($this->discount_type == 'flat') {
            $products_discount = $this->get_flat($products_total, $table_id);
          }
          break;
        case 'quantity':
          if ($this->discount_type == 'percentage') {
            $percentage = $this->get_percentage($products_qty, $table_id);
          } elseif ($this->discount_type == 'flat') {
            $products_discount = $this->get_flat($products_qty, $table_id);
          }
          break;
      }
      if ($this->discount_type == 'percentage') {  
        $products_discount = $products_total * ($percentage / 100); // $50 * 0.10 = $5.00
      } // else leave as flat discount
      if ($this->recalculate_tax == 'true') {
        $products_tax_discount = $this->get_tax_discount($products_id, $products_discount);
        $this->tax_discount += $products_tax_discount;
        // add the tax to the discount if price is displaying with tax as discount is calculated on net price, not with tax
        //if (DISPLAY_PRICE_WITH_TAX == 'true') {
          //$products_discount += $products_tax_discount;
        //}
      }
      return $products_discount;
    }
    
    function get_tax_class_id_from_description($description) {
      global $db;
      $tax_query = "SELECT * FROM " . TABLE_TAX_RATES . " WHERE tax_description = '" . $description . "' LIMIT 1";
      $tax = $db->Execute($tax_query);
      $tax_class_id = $tax->fields['tax_class_id'];
      return (int)$tax_class_id;
    }
    
    function get_tax_rate($tax_class_id, $description) {
      global $db;
      $tax_query = "SELECT * FROM " . TABLE_TAX_RATES . " 
                    WHERE tax_description = '" . $description . "'
                    AND tax_class_id = " . (int)$tax_class_id . " 
                    LIMIT 1";
      $tax = $db->Execute($tax_query);
      $tax_rate = $tax->fields['tax_rate'] / 100;
      return $tax_rate;
    }
    function pr($val){
      if(is_array($val)){
        var_dump($val);
      }else{
        echo $val;
      }
    }
    function get_percentage($measure, $table_id) {
      $table_cost = $this->multi_explode(',', ':', constant('MODULE_CATEGORY_DISCOUNT_TABLE_' . $table_id . $this->wholesale));
      $percentage = 0;
      for ($i=0; $i<count($table_cost); $i+=2) {
        //$this->pr($table_cost);exit;
        if ($measure >= $table_cost[$i]) {
          $percentage = $table_cost[$i+1]; // 10 / 100 = 0.10
          //break;
        }
      }
      return $percentage;
    }
    
    function get_flat($measure, $table_id) {
      $table_cost = $this->multi_explode(',', ':', constant('MODULE_CATEGORY_DISCOUNT_TABLE_' . $table_id . $this->wholesale));
      for ($i=0; $i<count($table_cost); $i+=2) {
        if ($measure >= $table_cost[$i]) {
          $flat = $table_cost[$i+1]; // 10
          //break;
        }
      }
      return $flat;
    }
    
  function multi_explode($delim1, $delim2, $string) {
      $new_data = array();
      $data = explode($delim1, $string);
      foreach ($data as $key => $value) {
        $new_data = array_merge($new_data, explode($delim2, $value));
    }
    return $new_data;
    }         
        
    function check(){
        global $db;
        if(!isset($this->_check)){
          $check_query  = $db->Execute("SELECT configuration_value FROM ". TABLE_CONFIGURATION ." WHERE configuration_key = 'MODULE_CATEGORY_DISCOUNT_STATUS'");
          $this->_check = $check_query->RecordCount();
          if ($this->_check && defined('MODULE_CATEGORY_DISCOUNT_VERSION')) { 
            switch(MODULE_CATEGORY_DISCOUNT_VERSION) {
              case '1.1.0':
                $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.1.1' WHERE configuration_key = 'MODULE_CATEGORY_DISCOUNT_VERSION' LIMIT 1;");
                // do not break and continue to the next version
              case '1.1.1':
                $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.1.2' WHERE configuration_key = 'MODULE_CATEGORY_DISCOUNT_VERSION' LIMIT 1;");
                break;                       
            }
          } elseif ($this->_check) {
            if(!defined('MODULE_CATEGORY_DISCOUNT_VERSION')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Version', 'MODULE_CATEGORY_DISCOUNT_VERSION', '1.1.0', 'Version Installed', '6', '1', now())");
          }
        }
        return $this->_check;
    }

    function keys() {
      $keys = array(
                    'MODULE_CATEGORY_DISCOUNT_VERSION', 
                    'MODULE_CATEGORY_DISCOUNT_STATUS', 
                    'MODULE_CATEGORY_DISCOUNT_SORT_ORDER', 
                    'MODULE_CATEGORY_DISCOUNT_CALC_TAX',
                    'MODULE_CATEGORY_DISCOUNT_TYPE',
                    'MODULE_CATEGORY_DISCOUNT_CALC_TYPE',
                    'MODULE_CATEGORY_CALCULATION_TYPE',
                    'MODULE_CATEGORY_DISCOUNT_TYPE_W',
                    'MODULE_CATEGORY_DISCOUNT_CALC_TYPE_W',
                    'MODULE_CATEGORY_CALCULATION_TYPE_W',
                    'MODULE_CATEGORY_DISCOUNT_ALERT',
                    'MODULE_CATEGORY_DISCOUNT_SORT_ORDER'
                  );
      for ($j=1; $j<=$this->groups; $j++) {
        $keys[] = 'MODULE_CATEGORY_DISCOUNT_CATEGORIES_' . $j;
        $keys[] = 'MODULE_CATEGORY_DISCOUNT_TABLE_' . $j;
        $keys[] = 'MODULE_CATEGORY_DISCOUNT_TABLE_' . $j . '_W';
      }
      return $keys;
    }

    function install() {
      global $db;

      
      if(!defined('MODULE_CATEGORY_DISCOUNT_GROUPS')) $db->Execute("INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES (NULL, 'Category Discount Groups', 'MODULE_CATEGORY_DISCOUNT_GROUPS', '3', 'How many groups of categories are needed for category discounts?', 6, 99, NOW(), NOW(), NULL, NULL);");

      if(!defined('MODULE_CATEGORY_DISCOUNT_VERSION')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Version', 'MODULE_CATEGORY_DISCOUNT_VERSION', '1.1.2', 'Version Installed', '6', '1', now())");
      if(!defined('MODULE_CATEGORY_DISCOUNT_STATUS')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Activate', 'MODULE_CATEGORY_DISCOUNT_STATUS', 'true', 'Do you want to enable Category Discounts?', '6', '1','zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
      if(!defined('MODULE_CATEGORY_DISCOUNT_CALC_TAX')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Re-Calculate Tax', 'MODULE_CATEGORY_DISCOUNT_CALC_TAX', 'true', 'Re-calculate tax on discounted amount?', '6', '4','zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
      if(!defined('MODULE_CATEGORY_DISCOUNT_TYPE')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Discount Base', 'MODULE_CATEGORY_DISCOUNT_TYPE', 'price', 'Calculate discount on price or quantity?', '6', '5','zen_cfg_select_option(array(\'price\', \'quantity\'), ', now())");      
      if(!defined('MODULE_CATEGORY_DISCOUNT_TYPE_W')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Wholesale Discount Base', 'MODULE_CATEGORY_DISCOUNT_TYPE_W', 'price', 'Calculate wholesale discount on price or quantity?', '6', '5','zen_cfg_select_option(array(\'price\', \'quantity\'), ', now())");      
      if(!defined('MODULE_CATEGORY_CALCULATION_TYPE')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Calculation Type', 'MODULE_CATEGORY_CALCULATION_TYPE', 'individual', 'Calculate discount on sum of category or individual products within category?', '6', '6','zen_cfg_select_option(array(\'individual\', \'sum\'), ', now())");
      if(!defined('MODULE_CATEGORY_CALCULATION_TYPE_W')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Wholesale Calculation Type', 'MODULE_CATEGORY_CALCULATION_TYPE_W', 'individual', 'Calculate wholesale discount on sum of category or individual products within category?', '6', '6','zen_cfg_select_option(array(\'individual\', \'sum\'), ', now())");
      if(!defined('MODULE_CATEGORY_DISCOUNT_CALC_TYPE')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Discount Type', 'MODULE_CATEGORY_DISCOUNT_CALC_TYPE', 'percentage', 'Should the discount be given as a percentage or a flat rate?', '6', '7','zen_cfg_select_option(array(\'percentage\', \'flat\'), ', now())");
      if(!defined('MODULE_CATEGORY_DISCOUNT_CALC_TYPE_W')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Wholesale Discount Type', 'MODULE_CATEGORY_DISCOUNT_CALC_TYPE_W', 'percentage', 'Should the wholesale discount be given as a percentage or a flat rate?', '6', '7','zen_cfg_select_option(array(\'percentage\', \'flat\'), ', now())");
      if(!defined('MODULE_CATEGORY_DISCOUNT_ALERT')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Display Alert', 'MODULE_CATEGORY_DISCOUNT_ALERT', 'false', 'Display a Javascript alert message on the shopping cart page when the discount changes?', '6', '20','zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
      if(!defined('MODULE_CATEGORY_DISCOUNT_SORT_ORDER')) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_CATEGORY_DISCOUNT_SORT_ORDER', '55', 'Sort order of display.', '6', '99', now())");
      $groups = ($this->groups > 0) ? $this->groups : $this->groups = MODULE_CATEGORY_DISCOUNT_GROUPS;
      
      for ($j = 1; $j <= $this->groups; $j++) {
        if(!defined('MODULE_CATEGORY_DISCOUNT_CATEGORIES_' . $j)) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Categories " . $j . "', 'MODULE_CATEGORY_DISCOUNT_CATEGORIES_" . $j . "', '1,2,3,4,5', 'For what category ids should the module discount (leave blank for all categories)?', '6', '50', 'zen_cfg_textarea(', now())");
        if(!defined('MODULE_CATEGORY_DISCOUNT_TABLE_' . $j)) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Discount Percentage " . $j . "', 'MODULE_CATEGORY_DISCOUNT_TABLE_" . $j . "', '100:7.5,250:10,500:12.5,1000:15', 'Set the price/quantity breaks and discount percentages', '6', '50', 'zen_cfg_textarea(', now())");      
        if(!defined('MODULE_CATEGORY_DISCOUNT_TABLE_' . $j)) $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Wholesale Discount Percentage " . $j . "', 'MODULE_CATEGORY_DISCOUNT_TABLE_" . $j . "_W', '100:7.5,250:10,500:12.5,1000:15', 'Set the price/quantity breaks and discount percentages', '6', '50', 'zen_cfg_textarea(', now())");
      }
    }
    
    function remove() {
      global $db;
      $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
      $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_CATEGORY_DISCOUNT_GROUPS'");
    }
  }