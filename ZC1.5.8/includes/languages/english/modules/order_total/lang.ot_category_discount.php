<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_category_discount.php 10 2008-06-02 14:30:11Z numinix $
 */

 $define = [
    'MODULE_CATEGORY_DISCOUNT_TITLE' => (IS_ADMIN_FLAG) ? 'Category Discount' : ucwords(MODULE_CATEGORY_DISCOUNT_TYPE) . ' Discount',
    'MODULE_CATEGORY_DISCOUNT_DESCRIPTION' => (IS_ADMIN_FLAG) ? 'Category Discount' : ucwords(MODULE_CATEGORY_DISCOUNT_TYPE) . ' Discount',
    'SHIPPING_NOT_INCLUDED' => ' [Shipping not included]',
    'TAX_NOT_INCLUDED' => ' [Tax not included]'
  ];

  return $define;
?>