<?php
/**
 * @package order_total
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: category_discount.php 10 2008-06-02 14:30:11Z numinix $
 */
 
 define('TEXT_CATEGORY_DISCOUNT_PRODUCT', 'This product is eligible for a %s sensitive discount.');
 define('TEXT_CATEGORY_DISCOUNT', 'Your purchase may be eligible for a %s sensitive discount of <font color="green"><strong>%s</strong></font>! Please continue to the <a href="%s">CHECKOUT</a> for your updated order total.');
 
 ?>